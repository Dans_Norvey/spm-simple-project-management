
# About SPM

##SPM : Simple Project Management

SPM is a console application toolbox to manage projects and task.

## Contributing

Thank you for considering contributing to the SPM Toolbox! Contact me about different ways to collaborate.

## Security Vulnerabilities

If you discover a security vulnerability within this sofware, please contact me via [drocks2000@gmail.com](mailto:drocks2000@gmail.com).
All security vulnerabilities will be promptly addressed.

## License

This software is open-sourced software licensed under the [Creative Common 4.0 BY-ND-NC](https://opensource.org/licenses/MIT).
