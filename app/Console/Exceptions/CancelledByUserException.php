<?php

namespace App\Console\Exceptions;

/**
 * Class CancelledByUserException
 * 
 * @package App\Console\Exceptions
 */
class CancelledByUserException extends \Exception
{
}