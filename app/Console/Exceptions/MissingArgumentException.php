<?php

namespace App\Console\Exceptions;

/**
 * Class MissingArgumentException
 *
 * @package App\Console\Exceptions
 */
class MissingArgumentException extends \Exception
{

}