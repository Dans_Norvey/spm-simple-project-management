<?php

namespace App\Console;

use Illuminate\Console\Command;

/**
 * Class BaseCommand
 *
 * @package App\Console
 */
class BaseCommand extends Command
{
    const CANCELLED_BY_USER = 100;
}