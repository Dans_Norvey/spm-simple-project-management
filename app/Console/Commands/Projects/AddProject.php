<?php

namespace App\Console\Commands\Projects;

use App\Console\BaseCommand;
use App\Console\Exceptions\CancelledByUserException;
use App\Exceptions\InvalidDateException;
use App\Managers\ProjectManager;

/**
 * Class AddProject
 *
 * @package App\Console\Commands\Projects
 */
class AddProject extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a new project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    /**
     * @return int
     *
     * @throws \Throwable
     */
    public function handle()
    {
        $status = 0;

        $this->info("Create Project");
        $this->info("==============");

        $projectData = [
            'name' => null,
            'description' => null,
            'start_date' => null,
            'due_date' => null,
        ];
        try {
            $projectData['name'] = $this->requestName();
            $projectData['description'] = $this->requestDescription();
            $projectData['start_date'] = $this->requestIssueDate();
            $projectData['due_date'] = $this->requestDueDate();
            $manager = new ProjectManager();
            $manager->addProject($projectData);
            $this->info("* success *");
            $this->info(print_r($projectData, true));
        } catch (CancelledByUserException $e) {
            $this->warn("* cancelled by user *");
            $status = self::CANCELLED_BY_USER;
        }

        return $status;

    }

    /**
     * @return string
     *
     * @throws CancelledByUserException
     */
    private function requestName(): string
    {
        $name = null;

        while (empty($name)) {
            $name = $this->ask("Project name (empty to cancel): ");
            if (empty($name)) {
                    throw new CancelledByUserException();
            }
            if (! $this->isUniqueProjectName($name)) {
                $this->warn("Project name is taken. Please, give another name.");
                $name = null;
            }
        }

        return $name;
    }

    /**
     * @return string
     */
    private function requestDescription()
    {
        return $this->ask("Description: ");
    }

    /**
     * @return \DateTime|null
     */
    private function requestIssueDate()
    {
        $startDate = null;

        $date = false;
        while ($date === false) {
            $date = $this->ask("Start date (y-m-d). Use 'now' for today or empty to ignore start date");
            try {
                $startDate = (empty($date))
                    ? null
                    : $this->convertDate($date);
                $date = true;
            } catch (InvalidDateException $e) {
                $this->warn("Invalid date. Please, give a valid date.");
                $date = false;
            }
        }

        return $startDate;
    }

    /**
     * @return \DateTime
     */
    private function requestDueDate()
    {
        $dueDate = null;

        $date = false;
        while ($date === false) {
            $date = $this->ask("Due date (y-m-d). Use 'now' for today or empty to ignore start date");
            try {
                $dueDate = (empty($date))
                    ? null
                    : $this->convertDate($date);
                $date = true;
            } catch (InvalidDateException $e) {
                $this->warn("Invalid date. Please, give a valid date.");
                $date = false;
            }
        }

        return $dueDate;
    }


    /**
     * @param string $name
     *
     * @return bool
     */
    private function isUniqueProjectName($name) : bool
    {
        return true;
    }

    /**
     * @param string $dateString
     *
     * @return \DateTime
     *
     * @throws InvalidDateException
     */
    private function convertDate($dateString) : \DateTime
    {
        $date = null;

        if ($dateString == 'now') {
            $date = new \DateTime('today');
        } else {
            $dateString = str_replace('/', '-', $dateString);
            $parts = explode('-', $dateString);

            if (is_array($parts) && count($parts) === 3 && checkdate($parts[1], $parts[2], $parts[0])) {
                $date = \DateTime::createFromFormat("Y-m-d", sprintf("%s", $dateString));
            }

            if ($date instanceof \DateTime) {
                $date->setTime(0, 0, 0);
            } else {
                throw new InvalidDateException();
            }
        }

        return $date;
    }
}
