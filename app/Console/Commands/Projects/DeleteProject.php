<?php

namespace App\Console\Commands\Projects;

use App\Console\BaseCommand;
use App\Console\Exceptions\MissingArgumentException;
use App\Exceptions\ProjectNotFoundException;
use App\Managers\ProjectManager;
use App\Models\Project;

/**
 * Class DeleteProject
 *
 * @package App\Console\Commands\Projects
 */
class DeleteProject extends BaseCommand
{
    private $delete_forced = false;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:delete
                            {--f|force : force deletion}
                            {projectId? : Id of project to be deleted}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an existing project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $status = -1;

        $this->checkForceDelete();

        try {
            $projectId = $this->argument('projectId');
            if (empty($projectId)) {
                throw new MissingArgumentException();
            }

            $manager = new ProjectManager();
            $project = $manager->findProjectById(intval($projectId));
            if (! $project instanceof Project) {
                throw new ProjectNotFoundException();
            }

            $msgPattern = 'Project "%s" will be deleted. Are you sure?';
            $shouldDelete = $this->isDeleteForced()
                || $this->confirm(sprintf($msgPattern, $project->getName()));

            if ($shouldDelete) {
                $project->delete();
                $this->info("Project has been deleted");
                $status = 0;

            } else {
                $this->warn("Cancelled by user");
                $status = self::CANCELLED_BY_USER;
            }

        } catch (ProjectNotFoundException $e) {
            $this->error("ERROR: Project not found.");

        } catch (MissingArgumentException $e) {
            $this->warn("Please, select one project to delete");

        } catch (\Exception $e) {
            $this->error(sprintf("Error deleting project(l.%d [errcode=%d] %s)",
                $e->getLine(), $e->getCode(), $e->getMessage()));
        }

        return $status;
    }

    /**
     *
     */
    private function checkForceDelete()
    {
        $this->delete_forced = $this->hasOption('force')
                                && ($this->option('force') == 1);
    }

    /**
     * @return bool
     */
    private function isDeleteForced() : bool
    {
        return $this->delete_forced;
    }
}
