<?php

namespace App\Console\Commands\Projects;

use App\Managers\ProjectManager;
use Illuminate\Console\Command;

/**
 * Class ProjectList
 *
 * @package App\Console\Commands\Project
 */
class ListProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all projects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return integer
     */
    public function handle()
    {
        $pm = new ProjectManager();
        $projects = $pm->getAllProjects();

        $this->info("Project List");
        $this->info("============");
        $this->info(sprintf(" %-4s | %-35s | %-10s | %-10s",
            ' Id ', 'Project name', 'Start date', ' Due date'));
        $this->info(sprintf("%'-5s | %'-35s | %'-10s | %'-10s" , '', '', '', ''));
        if ($projects->isNotEmpty()) {
            foreach ($projects as $project) {
                $info = sprintf("%5d | %-35s | %-10s | %-10s",
                    $project->getId(),
                    $project->getName(),
                    ($project->getStartDate() instanceof \DateTime) ? $project->getStartDate()->format("Y-m-d") : '--',
                    ($project->getDueDate() instanceof \DateTime) ? $project->getDueDate()->format("Y-m-d") : '--'
                );
                $this->info($info);
            }
        } else {
            $this->info("-No projects-");
        }

        return 0;
    }
}
