<?php

namespace App\Managers;

use App\Interfaces\IProject;
use App\Models\Project;

/**
 * Class ProjectManager
 *
 * @package App\Managers
 */
class ProjectManager
{
    /**
     * @return Project[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllProjects()
    {
        return Project::all();
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function addProject($data)
    {
        $added = false;
        try {
            $project = new Project($data);
            $added = $project->saveOrFail();
        } catch (\Exception $e) {
            // do nothing
        } catch (\Throwable $e) {
            // do nothing
        }

        return $added;
    }


    /**
     * @param int $projectId
     *
     * @return IProject
     */
    public function findProjectById(int $projectId)
    {
        $fdm = new Project();
        $project = $fdm->find($projectId);

        return $project;
    }
}