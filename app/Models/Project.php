<?php

namespace App\Models;

use App\Interfaces\IProject;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 *
 * @package App\Models
 */
class Project extends Model implements IProject
{
    protected $table = 'tasks';

    protected $fillable = [
        'name',
        'description',
        'start_date',
        'due_date'
    ];

    protected $dates = [
        'start_date',
        'due_date',
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->getKey();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute('name');
    }

    /**
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->getAttribute('start_date');
    }

    /**
     * @return \DateTime|null
     */
    public function getDueDate()
    {
        return $this->getAttribute('due_date');
    }
}
