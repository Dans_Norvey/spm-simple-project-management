<?php

namespace App\Exceptions;

/**
 * Class InvalidDateException
 * 
 * @package App\Exceptions
 */
class InvalidDateException extends \Exception
{
}