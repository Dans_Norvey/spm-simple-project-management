<?php

namespace App\Interfaces;


/**
 * Interface IProject
 *
 * @package App\Interfaces
 */
interface IProject
{
    /**
     * @return int
     */
    public function getId() : int;

    /**
     * @return string
     */
    public function getName() : string;

    /**
     * @return \DateTime|null
     */
    public function getStartDate();

    /**
     * @return \DateTime|null
     */
    public function getDueDate();
}